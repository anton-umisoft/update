<?php
require_once __DIR__ . '/vendor/autoload.php';

// Список слов не для транслита, оставть их как есть.
$notTranslate = array('market');

$options = getopt('', array('name:', 'dry-run::', 'no-ansi::'));

$style = function ($text) use($options) {
	return isset($options['no-ansi']) ? '' : $text;
};

$slugify = new Cocur\Slugify\Slugify();

$i = 0; // Индетификатор безымянной страницы. Используем его в рамках одного сайта (коллизий не будет).
$translit = function ($text) use ($slugify, &$i) {
	if ($text === '') {
		// Если название страницы пустое (ввиду особоновлей UMI.CMS) будем давать свое имя таким страницам.
		$collisionDefender = base_convert(++$i, 10, 36); // Давайте ещё использовать буквы! 10 цифр + 26 букв = 36. 
		return 'no-name-' . $collisionDefender;
	} else {
		return $slugify->slugify($text, '_');
	}
};

require_once __DIR__ . "/../../data/host/$options[name]/root/standalone.php";

$pages = new selector('pages');
$pages->where('is_active')->equals(array(0, 1)); // Выбираем все страницы, по умолчанию только активные.
$pages->option('return', array('id')); // Загружаем только ID шники страниц, объекты создаёт потом.

foreach ($pages as $row) {
	$page = umiHierarchy::getInstance()->getElement($row['id'], true);

	echo $page->name . ": " . str_replace($page->altName, $style("\033[0;41m") . $page->altName . $style("\033[0m"), $page->link) . " -> ";
	
	// Проверяем нужно ли транслитить или пропустить перевод ссылки.
	if (in_array($page->altName, $notTranslate, true) || $page->link === '/') {
		
		echo $style("\033[0;34m") . "skip" . $style("\033[0m") . "\n";
		
	} else {

		if (isset($options['dry-run'])) {
			$oldAltName = $page->altName;
			$newAltName = $translit($page->name);
			echo str_replace($oldAltName, $style("\033[0;42m") . $newAltName . $style("\033[0m"), $page->link) . "\n";
		} else {
			$oldAltName = $page->altName;
			$page->setAltName($translit($page->name), false);
			echo str_replace($oldAltName, $style("\033[0;42m") . $page->altName . $style("\033[0m"), $page->link) . "\n";
		}

	}

	
	// Удаляем мета теги: заголовок, описание, ключевые слова. 
	
	$title = $page->title;
	if (!empty($title)) {
		echo "    Erase title " . $style("\033[0;41m") . $title . $style("\033[0m") . "\n";

		if (!isset($options['dry-run'])) {
			$page->title = '';
		}
	}

	$metaKeywords = $page->getValue('meta_keywords');
	if (!empty($metaKeywords)) {
		echo "    Erase meta keywords " . $style("\033[0;41m") . $metaKeywords . $style("\033[0m") . "\n";

		if (!isset($options['dry-run'])) {
			$page->setValue('meta_keywords', '');
		}
	}

	$metaDescriptions = $page->getValue('meta_descriptions');
	if (!empty($metaDescriptions)) {
		echo "    Erase meta descriptions " . $style("\033[0;41m") . $metaDescriptions . $style("\033[0m") . "\n";

		if (!isset($options['dry-run'])) {
			$page->setValue('meta_descriptions', '');
		}
	}

	if (!isset($options['dry-run'])) {
		$page->commit();
	}
     
	// Выгружаем из памяти.
	umiHierarchy::getInstance()->unloadElement($row['id']);
}
