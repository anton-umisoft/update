<?php
/**
 * Скрипт для изменения псевдостатического имени страниц и очистки метатегов.
 *
 * Инструкция по применению:
 *  - Склонировать репозиторй в корне контроллера (git clone https://bitbucket.org/anton-umisoft/update.git)
 *  - Запустить `php update/run.php --dry-run` для тестового прогона (посмотреть что получится).
 *  - Запустить `php update/run.ph` для реального изменения ссылок.
 * 
 * Опция --no-dev запустит скрипт по обычным сайтам. Для запуска скрипта по dev сайтам указывать её НЕ НАДО!
 * Опция --no-ansi выключит "раскраску" вывода скрипта (Например для вывода в лог).
 */

$options = getopt('', array('dry-run::', 'no-dev::', 'no-ansi::'));

$style = function ($text) use($options) {
	return isset($options['no-ansi']) ? '' : $text;
};

$config = parse_ini_file(__DIR__ . '/../../conf/config.ini', true);

$portString = '';

if (isset($config['Database']['port'])) {
	$portString = "port={$config['Database']['port']};";
}

$connectionString = "mysql:host={$config['Database']['host']};{$portString}dbname={$config['Database']['dbname']}";

echo "Connect: $connectionString " . $config['Database']['user'] . "/" . $config['Database']['password'] . "\n";

$db = new PDO($connectionString, $config['Database']['user'], $config['Database']['password']);

// Выбираем все айты для разработки. 
$st = $db->query('SELECT * FROM host WHERE IsDevelop = ' . (isset($options['no-dev']) ? '0' : '1') . ' AND IsBlock = 0 AND isConfirmed = 1 AND IsHide = 0 AND isDeleted = 0 AND isUpdating = 0');

$rows = $st->fetchAll(PDO::FETCH_ASSOC);
$count = count($rows);
$i = 1;

foreach ($rows as $row) {
	$name = $row['Name'];

	echo "\n" . $style("\033[0;33m") . "[$i/$count] Working on $name" . $style("\033[0m") . "\n";
	
	$status = 0;
	$output = array();
	$dry = '';
	$noAnsi = '';

	if (isset($options['dry-run'])) {
		$dry = '--dry-run';
	}
	
	if (isset($options['no-ansi'])) {
		$noAnsi = '--no-ansi';
	}

	exec("php " . __DIR__ . "/update.php --name $name $dry $noAnsi", $output, $status);

	echo join("\n", $output);
	echo "\n" . $style("\033[0;36m") . "[$i/$count] Done on $name" . $style("\033[0m") . "\n";
	$i++;
}

echo "\n" . $style("\033[0;32m") . (isset($options['dry-run']) ? '* Dry run done' : 'Done') . $style("\033[0m") . "\n";
